﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace UltrasoundMonitor
{
    class FileManager
    {
        private uint NumberOfChanels = DataFlow.settings.NumberOfChanels;
        private int fileNumber = 0;

        public void refreshSettings()
        {
            Directory = DataFlow.settings.saveFilesDirectory;
            NumberOfChanels = DataFlow.settings.NumberOfChanels;
        }

        public void saveABDFile(double[,] averageBandpassDft, string suffix)
        {
            StringBuilder output = new StringBuilder();
            for (int j = 0; j < averageBandpassDft.GetLength(1); j++)
            {
                for (int i = 0; i < averageBandpassDft.GetLength(0); i++)
                {
                    output.Append(averageBandpassDft[i, j] + ";");
                }
                output.Append("\n");
            }
            string filePathLong = Directory + "abd_" + suffix + DateTime.Now.ToString("dd.MM.yyyy_HH.mm.ss") + "_" +
                                  fileNumber + ".csv";
            File.WriteAllText(filePathLong, output.ToString());
            Console.WriteLine("zapisano plik o nazwie: " + filePathLong);
        }

        public void savePISFile(int[,] pointsInSpace)
        {
            StringBuilder output = new StringBuilder();
            for (int j = 0; j < pointsInSpace.GetLength(1); j++)
            {
                for (int i = 0; i < pointsInSpace.GetLength(0); i++)
                {
                    output.Append(pointsInSpace[i, j] + ";");
                }
                output.Append("\n");
            }
            string filePathLong = Directory + "pis_" + DateTime.Now.ToString("dd.MM.yyyy_HH.mm.ss") + "_" + fileNumber +
                                  ".csv";
            File.WriteAllText(filePathLong, output.ToString());
            Console.WriteLine("zapisano plik o nazwie: " + filePathLong);
        }

        public void savePointsFile(List<int>[] points)
        {
            StringBuilder output = new StringBuilder();
            int maxlength = 0;
            for (int i = 0; i < points.Length; i++)
            {
                if (maxlength < points[i].Count)
                {
                    maxlength = points[i].Count;
                }
            }
            for (int j = 0; j < maxlength; j++)
            {
                for (int i = 0; i < points.Length; i++)
                {
                    if (points[i].Count > j)
                    {
                        output.Append(points[i][j] + ";");
                    }
                    else
                    {
                        output.Append(";");
                    }
                }
                output.Append("\n");
            }
            string filePathLong = Directory + "points_" + DateTime.Now.ToString("dd.MM.yyyy_HH.mm.ss") + "_" +
                                  fileNumber + ".csv";
            File.WriteAllText(filePathLong, output.ToString());
            Console.WriteLine("zapisano plik o nazwie: " + filePathLong);
        }

        public void saveStftFile(double[,,] input)
        {
            StringBuilder[] outptFile = new StringBuilder[input.GetLength(0)];
            Parallel.For(0, input.GetLength(0), i =>
            {
                outptFile[i] = new StringBuilder();
                for (int j = 0; j < input.GetLength(2); j++)
                {
                    for (int k = 0; k < input.GetLength(1); k++)
                    {
                        outptFile[i].AppendFormat("{0:0000.00000000};", input[i, k, j]);
                    }
                    outptFile[i].Append('\n');
                }
                string filePathLong = Directory + "stft_" + DateTime.Now.ToString("dd.MM.yyyy_HH.mm.ss") + "_chanel_" +
                                      i + "_" + fileNumber + ".csv";
                File.WriteAllText(filePathLong, outptFile[i].ToString());
                Console.WriteLine("zapisano plik o nazwie: " + filePathLong);
            });
        }

        public void saveSamplesFile(double[][] matrix, string sufix)
        {
            StringBuilder csvTemp = new StringBuilder();
            foreach (double[] t in matrix)
            {
                for (int j = 0; j < NumberOfChanels; j++)
                {
                    csvTemp.AppendFormat("{0,0:0000.00000000};", t[j]);
                }
                csvTemp.Append("\n");
            }
            string filePathLong = Directory + "samples_" + DateTime.Now.ToString("dd.MM.yyyy_HH.mm.ss") + "_" +
                                  fileNumber + "_" + sufix + ".csv";
            File.WriteAllText(filePathLong, csvTemp.ToString());
            Console.WriteLine("zapisano plik o nazwie: " + filePathLong);
        }

        public string Directory { set; get; } = DataFlow.settings.saveFilesDirectory;

        public int FileNumerInc()
        {
            return ++fileNumber;
        }
    }
}