﻿using System;
using System.Collections.Concurrent;
using System.IO;

namespace UltrasoundMonitor {
    class DataReader
    {
        private uint numberOfChanels = DataFlow.settings.NumberOfChanels;
        private uint numberOfSamples = DataFlow.settings.NumberOfSamples;
        private bool normalize = DataFlow.settings.NormalizeDataSamples;
        private string DemoModeFileName = DataFlow.settings.DemoModeFileName;
        private string DemoModeFileExtention = DataFlow.settings.DemoModeFileExtention;
        private string DemoModeFilePath = DataFlow.settings.DemoModeFilePath;
        private int DemoFileNumber = 0;

        public void refreshSettings()
        {
            numberOfChanels = DataFlow.settings.NumberOfChanels;
            numberOfSamples = DataFlow.settings.NumberOfSamples;
            normalize = DataFlow.settings.NormalizeDataSamples;
            DemoModeFileName = DataFlow.settings.DemoModeFileName;
            DemoModeFileExtention = DataFlow.settings.DemoModeFileExtention;
            DemoModeFilePath = DataFlow.settings.DemoModeFilePath;

        }

        public ConcurrentQueue<double[]> readDemoFile()
        {
            ConcurrentQueue<double[]> data = new ConcurrentQueue<double[]>();

            string[] files = Directory.GetFiles(DemoModeFilePath, DemoModeFileName);

            if (files.Length > 0)
            {
                DemoFileNumber = DemoFileNumber % files.Length;

                string[] lines = File.ReadAllLines(files[DemoFileNumber]);

                if (lines.Length > 0)
                {
                    foreach (string line in lines)
                    {
                        data.Enqueue(praseLineDemo(line));
                    }
                }

                DemoFileNumber++;
            }

            return data;
        }

        public double[] praseLine(string line)
        {
            double[] output = new double[numberOfChanels];
            string[] cels = line.Split(';');
            if (cels.Length != numberOfChanels) return output;
            for (int j = 0; j < numberOfChanels; j++)
            {
                output[j] = praseCell(cels[j]);
            }
            return output;
        }

        public double[] praseLineDemo(string line)
        {
            double[] output = new double[numberOfChanels];
            string[] cels = line.Split(';');

            for (int j = 0; j < numberOfChanels; j++)
            {
                output[j] = praseCellDemo(cels[j]);
            }
            return output;
        }

        private double praseCell(string input)
        {
            double output = 0;
            if (input.Length > 0)
            {
                try
                {
                    output = Convert.ToInt32(input, 16);
                }
                catch (Exception ex)
                {
                    output = 0;
                    Console.Error.WriteLine(ex);
                    Console.Error.WriteLine("błędny string to: " + input);
                }
            }
            if (output > 4096)
            {
                output = 0;
            }
            else
            {
                if (normalize)
                {
                    output = output / 4096;
                }
            }
            return output;
        }

        private double praseCellDemo(string input)
        {
            double output = 0;
            if (input.Length <= 0) return output;
            try
            {
                output = Convert.ToDouble(input);
            }
            catch (Exception ex)
            {
                output = 0;
                Console.Error.WriteLine(ex);
                Console.Error.WriteLine("błędny string to: " + input);
            }
            return output;
        }
    }
}
