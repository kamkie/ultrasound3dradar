﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace UltrasoundMonitor
{
    public class DataFlow
    {
        public static Settings settings = new Settings();
        public double[,,] stftMag;
        public double[][] dataFrame;
        public double[] avg;
        public double[,] averageBandpassDft;
        public double[,] averageBandpassRefDft;
        public double[] standardDeviation;
        public double[] averageBandDftLevel;
        public int[,] pointsInSpace;
        public List<int>[] points;
        public bool dataValid;
        public bool dataFresh;
        public bool continousDataProcces;
        public bool saveFiles;
        public List<double[]> PointsInSphericalCoordinates;

        private TransmisionManager transmisionManager = new TransmisionManager();
        private DspManager dspManager = new DspManager();
        private FileManager fileManager = new FileManager();
        private Task update;
        private uint NumberOfSamples = settings.NumberOfSamples;


        public DataFlow(bool saveFiles, bool continous)
        {
            continousDataProcces = continous;
            this.saveFiles = saveFiles;
            update = new Task(DataFlowUpdate);
            update.Start();
        }

        public void demoMode()
        {
            transmisionManager.demoMode();
        }

        public void realMode()
        {
            transmisionManager.realMode();
        }

        public void refreshSettings()
        {
            NumberOfSamples = settings.NumberOfSamples;
            dspManager.refreshSettings();
            fileManager.refreshSettings();
            transmisionManager.refreshSettings();
        }

        public void pause()
        {
            continousDataProcces = false;
            transmisionManager.pause();
            update = new Task(DataFlowUpdate);
            update.Start();
        }

        public void start()
        {
            continousDataProcces = true;
            transmisionManager.start();
            update = new Task(DataFlowUpdate);
            update.Start();
        }

        private void DataFlowUpdate()
        {
            do
            {
                try
                {
                    dataFrame = transmisionManager.getDataFrame();
                    if (saveFiles)
                    {
                        processDataConsole(dataFrame);
                    }
                    else
                    {
                        processDataWindow(dataFrame);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
                Thread.Sleep(10);
            } while (continousDataProcces);
        }

        private void processDataWindow(double[][] data)
        {
            avg = dspManager.calculateAvg(data);
            dspManager.filter(ref data, avg);
            stftMag = dspManager.computeStft(data);
            points = dspManager.detectPoints(stftMag, out averageBandpassDft, out averageBandpassRefDft,
                out pointsInSpace, out standardDeviation, out averageBandDftLevel);
            PointsInSphericalCoordinates = dspManager.pointsSphericalCoordinates(pointsInSpace, points);
            dataValid = true;
            dataFresh = true;
        }

        private void processDataConsole(double[][] data)
        {
            fileManager.FileNumerInc();

            avg = dspManager.calculateAvg(data);

            fileManager.saveSamplesFile(data, "un");
            dspManager.filter(ref data, avg);
            fileManager.saveSamplesFile(data, "filtred");

            stftMag = dspManager.computeStft(data);
            fileManager.saveStftFile(stftMag); //use to calibrate bandpass filter


            points = dspManager.detectPoints(stftMag, out averageBandpassDft, out averageBandpassRefDft,
                out pointsInSpace, out standardDeviation, out averageBandDftLevel);
            PointsInSphericalCoordinates = dspManager.pointsSphericalCoordinates(pointsInSpace, points);

            fileManager.saveABDFile(averageBandpassDft, "inband");
            fileManager.saveABDFile(averageBandpassRefDft, "outband");
            fileManager.savePISFile(pointsInSpace);
            fileManager.savePointsFile(points);

            dataValid = true;
            dataFresh = true;
        }
    }
}