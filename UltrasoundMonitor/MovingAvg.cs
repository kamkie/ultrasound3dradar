﻿namespace UltrasoundMonitor {
    class MovingAvg
    {
        private readonly double[] buffor;
        private readonly uint length;
        private uint index;
        private bool full;

        public MovingAvg(uint length)
        {
            this.length = length;
            index = 0;
            full = false;
            buffor = new double[length];
        }

        public void Add(double val)
        {
            buffor[index] = val;
            if (!full && (index == length - 1))
            {
                full = true;
            }
            index = (index + 1) % (length);
        }

        public double Avg
        {
            get
            {
                double avg = 0;
                uint count = full ? length : (index + 1);
                for (uint i = 0; i < count; i++)
                {
                    avg += buffor[i];
                }
                return avg / count;
            }
        }
    }
}
