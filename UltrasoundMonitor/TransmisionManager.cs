﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO.Ports;
using System.Threading.Tasks;


namespace UltrasoundMonitor
{
    class TransmisionManager
    {
        private BlockingCollection<ConcurrentQueue<double[]>> dataframes = new BlockingCollection<ConcurrentQueue<double[]>>();
        private string startPatern = DataFlow.settings.TransmisionStartPatern;
        private string stopPatern = DataFlow.settings.TransmisionStopPatern;
        private long watchDogsTicks = DataFlow.settings.WatchDogsTicks;
        private double PacketInvalidTreshold = DataFlow.settings.PacketInvalidTreshold;
        private uint NumberOfSamples = DataFlow.settings.NumberOfSamples;
        private DataReader dataReader = new DataReader();
        private SerialPort uart = new SerialPort();
        private ConcurrentQueue<double[]> data = new ConcurrentQueue<double[]>();
        private Task reConnection;
        private Stopwatch stopwatch = new Stopwatch();
        private bool WorkContinuous = DataFlow.settings.WorkContinuous;
        private bool IsInDemoMode = DataFlow.settings.IsInDemoMode;

        public void refreshSettings()
        {
            startPatern = DataFlow.settings.TransmisionStartPatern;
            stopPatern = DataFlow.settings.TransmisionStopPatern;
            watchDogsTicks = DataFlow.settings.WatchDogsTicks;
            PacketInvalidTreshold = DataFlow.settings.PacketInvalidTreshold;
            NumberOfSamples = DataFlow.settings.NumberOfSamples;
            WorkContinuous = DataFlow.settings.WorkContinuous;
            IsInDemoMode = DataFlow.settings.IsInDemoMode;
            dataReader.refreshSettings();
        }

        public void pause()
        {
            WorkContinuous = false;
            reConnection.Wait();
            if (reConnection.IsCompleted)
            {
                reConnection = new Task(reConnectionAction);
                reConnection.Start();
            }
        }

        public void start()
        {
            WorkContinuous = false;
            reConnection.Wait();
            WorkContinuous = true;
            if (reConnection.IsCompleted)
            {
                reConnection = new Task(reConnectionAction);
                reConnection.Start();
            }
        }

        public void demoMode()
        {
            IsInDemoMode = true;
        }

        public void realMode()
        {
            IsInDemoMode = false;
        }

        public TransmisionManager()
        {
            SerialPortInit();
            SerialPortOpen();
            reConnection = new Task(reConnectionAction);
            reConnection.Start();
            stopwatch.Start();
        }

        public double[][] getDataFrame()
        {
            return dataframes.Take().ToArray();
        }

        private void reConnectionAction()
        {
            do
            {
                if (IsInDemoMode)
                {
                    ConcurrentQueue<double[]> data = dataReader.readDemoFile();
                    dataframes.Add(data);
                    try
                    {
                        uart.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex);
                    }
                    System.Threading.Thread.Sleep(1000);
                }
                else
                {
                    SerialPortOpen();
                }
                System.Threading.Thread.Sleep(1000);
            } while (WorkContinuous);

        }

        private void SerialPortOpen()
        {
            if (stopwatch.ElapsedMilliseconds > watchDogsTicks)
            {
                try
                {
                    uart.Close();
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex);
                }
            }
            if (!uart.IsOpen)
            {
                string[] names = SerialPort.GetPortNames();
                if (names.Length > 0)
                {
                    try
                    {
                        uart.PortName = names[0];
                        uart.Open();
                        stopwatch.Restart();
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex);
                    }
                }
            }
        }

        private void SerialPortInit()
        {
            uart.DataReceived += DataReceivedHandler;
            uart.ErrorReceived += uart_ErrorReceived;
            uart.PinChanged += uart_PinChanged;
            uart.ReceivedBytesThreshold = 18;
            uart.DiscardNull = false;
            uart.ReadBufferSize = 2048;
        }

        private void uart_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            Console.WriteLine("pin się zmienił");
            SerialPort sp = (SerialPort)sender;
            sp.Close();
        }

        private void uart_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            Console.WriteLine("napotkano błąd");
            SerialPort sp = (SerialPort)sender;
            sp.Close();
        }

        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            string temp = "";
            while (sp.IsOpen && sp.BytesToRead > 12)
            {
                stopwatch.Restart();
                try
                {
                    temp = sp.ReadLine();
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.StackTrace);
                }
                if (temp.Equals(startPatern))
                {
                    data = new ConcurrentQueue<double[]>();
                }
                else if (temp.Equals(stopPatern))
                {
                    Console.WriteLine("Zgubiono pakietów: " + (NumberOfSamples - data.Count));
                    if (data.Count > NumberOfSamples * PacketInvalidTreshold && data.Count <= NumberOfSamples)
                    {
                        dataframes.Add(data);
                        if (!WorkContinuous)
                        {
                            try
                            {
                                uart.Close();
                            }
                            catch (Exception ex)
                            {
                                Console.Error.WriteLine(ex);
                            }
                        }
                    }
                }
                else
                {
                    data.Enqueue(dataReader.praseLine(temp));
                }
            }
        }

    }
}
