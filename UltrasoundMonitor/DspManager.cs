﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;
using MathNet.Numerics.IntegralTransforms;

namespace UltrasoundMonitor
{
    class DspManager
    {
        private uint bandStart;
        private uint bandStop;
        private double acceptanceThreshold;
        private uint MovingAvgLength;
        private uint DftLength;
        private uint overlapWindow;
        private bool removeAvgFromSamples;
        private bool useHammingWindow;
        private bool detectOnTime;
        private uint numberOfChanels;
        private double reciversTransmiterDistance;
        private uint soundSpeed;
        private uint samplingRate;
        private uint numberOfSamples;

        public DspManager()
        {
            refreshSettings();
        }

        public void refreshSettings()
        {
            acceptanceThreshold = DataFlow.settings.AcceptanceThreshold;
            MovingAvgLength = DataFlow.settings.MovingAvgLength;
            DftLength = DataFlow.settings.DftLength;
            samplingRate = DataFlow.settings.SamplingRate;
            bandStart = Convert.ToUInt32(DataFlow.settings.BandStart * DftLength / (double) samplingRate);
            bandStop = Convert.ToUInt32(DataFlow.settings.BandStop * DftLength / (double) samplingRate) + 1;
            overlapWindow = DataFlow.settings.OverlapWindow;
            removeAvgFromSamples = DataFlow.settings.RemoveAvgFromSamples;
            useHammingWindow = DataFlow.settings.UseHammingWindow;
            detectOnTime = DataFlow.settings.DetectOnTime;
            numberOfChanels = DataFlow.settings.NumberOfChanels;
            reciversTransmiterDistance = DataFlow.settings.ReciversTransmiterDistance;
            soundSpeed = DataFlow.settings.SoundSpeed;
            numberOfSamples = DataFlow.settings.NumberOfSamples;
        }

        public List<double[]> pointsSphericalCoordinates(int[,] pointsInSpace, List<int>[] points)
        {
            List<double[]> piontsInSC = new List<double[]>();
            double timeSegment;
            double timeSegmentDistance;
            CalculateCoefficients(pointsInSpace, out timeSegment, out timeSegmentDistance);
            List<double[]> distancesList = new List<double[]>(100);
            bool doContinue = true;
            int j = 0;
            do
            {
                double[] distances = new double[4];
                for (int i = 0; i < 4; i++)
                {
                    if (points[i].Count > j)
                    {
                        distances[i] = points[i][j] * timeSegmentDistance;
                    }
                    else
                    {
                        doContinue = false;
                    }
                }
                j++;
                distancesList.Add(distances);
            } while (doContinue);

            Parallel.For(0, distancesList.Count, i =>
            {
                double[] temp = distanceToSpherical(distancesList[i]);
                if (!Double.IsNaN(temp[0]) && !Double.IsNaN(temp[1]) && !Double.IsNaN(temp[2]))
                {
                    piontsInSC.Add(temp);
                }
            });

            return piontsInSC;
        }

        private double[] distanceToSpherical(double[] distance)
        {
            double[] coordinates = new double[3];
            double avgDdstance = (distance[0] + distance[1] + distance[2] + distance[3]) / 8;

            distance[0] -= avgDdstance;
            distance[1] -= avgDdstance;
            distance[2] -= avgDdstance;
            distance[3] -= avgDdstance;

            double x1, x2, y1, y2, p, r, alpha, beta;

            p = distance[0];
            r = distance[2];

            x1 = (0.25 *
                  Math.Sqrt(-16 * Math.Pow(reciversTransmiterDistance, 4) +
                            8 * p * p * reciversTransmiterDistance * reciversTransmiterDistance +
                            8 * r * r * reciversTransmiterDistance * reciversTransmiterDistance - Math.Pow(p, 4) -
                            Math.Pow(r, 4) + 2 * p * p * r * r)) / reciversTransmiterDistance;
            y1 = (0.25 * (p * p - r * r)) / reciversTransmiterDistance;

            alpha = Math.Atan2(x1, y1);

            p = distance[1];
            r = distance[3];

            x2 = (0.25 *
                  Math.Sqrt(-16 * Math.Pow(reciversTransmiterDistance, 4) +
                            8 * p * p * reciversTransmiterDistance * reciversTransmiterDistance +
                            8 * r * r * reciversTransmiterDistance * reciversTransmiterDistance - Math.Pow(p, 4) -
                            Math.Pow(r, 4) + 2 * p * p * r * r)) / reciversTransmiterDistance;
            y2 = (0.25 * (p * p - r * r)) / reciversTransmiterDistance;

            beta = Math.Atan2(x2, y2);


            avgDdstance = (Math.Sqrt(x1 * x1 + y1 * y1) + Math.Sqrt(x2 * x2 + y2 * y2)) / 2;

            coordinates[0] = avgDdstance;
            coordinates[1] = alpha;
            coordinates[2] = beta - Math.PI / 2;
            return coordinates;
        }

        private void CalculateCoefficients(int[,] pointsInSpace, out double timeSegment, out double timeSegmentDistance)
        {
            int length = pointsInSpace.GetLength(1);
            double time = (1 / (double) samplingRate) * numberOfSamples;
            timeSegment = time / length;
            timeSegmentDistance = soundSpeed * timeSegment;
        }

        public List<int>[] detectPoints(double[,,] stftMag, out double[,] averageBandpassDft,
            out double[,] averageBandpassRefDft, out int[,] pointsInSpace, out double[] standardDeviation,
            out double[] averageLevel)
        {
            List<int>[] points = new List<int>[stftMag.GetLength(0)];
            double[,] averageBandpassDftTemp = new double[stftMag.GetLength(0), stftMag.GetLength(2)];
            double[,] averageBandpassRefDftTemp = new double[stftMag.GetLength(0), stftMag.GetLength(2)];
            int[,] pointsInSpaceTemp = new int[stftMag.GetLength(0), stftMag.GetLength(2)];
            double[] standardDeviationTemp = new double[stftMag.GetLength(0)];
            double[] averageLevelTemp = new double[stftMag.GetLength(0)];

            Parallel.For(0, stftMag.GetLength(0), i =>
            {
                points[i] = new List<int>();
                averageLevelTemp[i] = 0;
                standardDeviationTemp[i] = 0;
                double averageRefLevel = 0;

                for (uint j = 0; j < stftMag.GetLength(2); j++)
                {
                    for (uint k = bandStart; k < bandStop; k++)
                    {
                        averageBandpassDftTemp[i, j] = stftMag[i, k, j];
                    }
                    if (detectOnTime)
                    {
                        averageLevelTemp[i] += averageBandpassDftTemp[i, j] / stftMag.GetLength(2);
                    }
                    else
                    {
                        for (int k = 5; k < stftMag.GetLength(1) - 5; k++)
                        {
                            averageBandpassRefDftTemp[i, j] = stftMag[i, k, j];
                        }
                        averageRefLevel += averageBandpassRefDftTemp[i, j] / stftMag.GetLength(2);
                    }
                }
                for (int j = 0; j < stftMag.GetLength(2); j++)
                {
                    standardDeviationTemp[i] += (averageBandpassDftTemp[i, j] - averageLevelTemp[i]) *
                                                (averageBandpassDftTemp[i, j] - averageLevelTemp[i]) /
                                                stftMag.GetLength(2);
                }
                standardDeviationTemp[i] = Math.Sqrt(standardDeviationTemp[i]);
                bool previousPoint = false;
                for (int j = 0; j < stftMag.GetLength(2); j++)
                {
                    if ((detectOnTime &&
                         averageBandpassDftTemp[i, j] >
                         averageLevelTemp[i] + standardDeviationTemp[i] * acceptanceThreshold) ||
                        (!detectOnTime &&
                         averageBandpassDftTemp[i, j] > averageBandpassRefDftTemp[i, j] * acceptanceThreshold))
                    {
                        if (!previousPoint)
                        {
                            points[i].Add(j);
                            pointsInSpaceTemp[i, j] = 1;
                        }
                        else
                        {
                            pointsInSpaceTemp[i, j] = 0;
                        }
                        previousPoint = true;
                    }
                    else
                    {
                        pointsInSpaceTemp[i, j] = 0;
                        previousPoint = false;
                    }
                }
            });
            averageBandpassDft = averageBandpassDftTemp;
            averageBandpassRefDft = averageBandpassRefDftTemp;
            pointsInSpace = pointsInSpaceTemp;
            standardDeviation = standardDeviationTemp;
            averageLevel = averageLevelTemp;
            return points;
        }

        public void filter(ref double[][] matrix, double[] avg)
        {
            double[][] matrixTemp = matrix;
            Parallel.For(0, numberOfChanels, i =>
            {
                MovingAvg mAvg = new MovingAvg(MovingAvgLength);
                int N = matrixTemp.Length;
                for (int j = 0; j < N; j++)
                {
                    if (removeAvgFromSamples)
                    {
                        matrixTemp[j][i] = matrixTemp[j][i] - avg[i];
                    }
                    mAvg.Add(matrixTemp[j][i]);
                    matrixTemp[j][i] = mAvg.Avg;
                }
            });
            matrix = matrixTemp;
        }

        public double[,,] computeStft(double[][] matrix)
        {
            //DiscreteFourierTransform fft = new DiscreteFourierTransform();
            Complex[][] samples = new Complex[numberOfChanels][];
            int samplesToCalculate = matrix.Length;
            long stftNumber = samplesToCalculate / (DftLength - overlapWindow);
            double[,,] output = new double[numberOfChanels, DftLength, stftNumber];
            double[] hammingWindow = HamingWindowGenerator(DftLength);
            Parallel.For(0, numberOfChanels, i =>
            {
                for (long k = 0; k < stftNumber; k++)
                {
                    samples[i] = new Complex[DftLength];
                    for (int j = 0; j < DftLength; j++)
                    {
                        if (k * (DftLength - overlapWindow) + j >= samplesToCalculate)
                        {
                            samples[i][j] = 0;
                        }
                        else
                        {
                            samples[i][j] = matrix[k * (DftLength - overlapWindow) + j][i];
                        }
                        if (useHammingWindow)
                        {
                            samples[i][j] = samples[i][j] * hammingWindow[j];
                        }
                    }
                    //fft.Radix2Forward(samples[i], MathNet.Numerics.IntegralTransforms.FourierOptions.NoScaling);
                    MathNet.Numerics.IntegralTransforms.Fourier.BluesteinForward(samples[i], FourierOptions.Default);
                    for (int j = 0; j < DftLength; j++)
                    {
                        output[i, j, k] = samples[i][j].Magnitude;
                    }
                }
            });
            return output;
        }

        private double[] dft(double[] data)
        {
            int n = data.Length;
            int m = n; // I use m = n / 2d;
            double[] real = new double[n];
            double[] imag = new double[n];
            double[] result = new double[m];
            double pi_div = 2.0 * Math.PI / n;
            for (int w = 0; w < m; w++)
            {
                double a = w * pi_div;
                for (int t = 0; t < n; t++)
                {
                    real[w] += data[t] * Math.Cos(a * t);
                    imag[w] += data[t] * Math.Sin(a * t);
                }
                result[w] = Math.Sqrt(real[w] * real[w] + imag[w] * imag[w]) / n;
            }
            return result;
        }

        private double[] HamingWindowGenerator(uint N)
        {
            double[] window = new double[N];
            for (uint i = 0; i < N; i++)
            {
                window[i] = HammingWindow(i, N);
            }
            return window;
        }

        private double HammingWindow(uint n, uint N)
        {
            return 0.54 - 0.46 * Math.Cos((2 * Math.PI * n) / (N - 1));
        }

        public double[] calculateAvg(double[][] matrix)
        {
            double[] avg = new double[numberOfChanels];
            double matrixLength = matrix.Length;
            Parallel.For(0, numberOfChanels, i =>
            {
                avg[i] = 0;
                for (int j = 0; j < matrixLength; j++)
                {
                    avg[i] += matrix[j][i] / matrixLength;
                }
            });
            Console.WriteLine("srednie wartości w kanałach wynoszą: {0,5}\t{1,5}\t{2,5}\t{3,5}\t",
                new Object[] {avg[0], avg[1], avg[2], avg[3]});
            return avg;
        }
    }
}