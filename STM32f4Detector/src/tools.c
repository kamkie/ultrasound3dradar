
#include "tools.h"

void Delay(__IO uint32_t nTick)
{
  for(; nTick != 0; nTick--);
}
