/******************** (C) COPYRIGHT 2009 STMicroelectronics ********************
* File Name          : main.c
* Author             : MCD Application Team
* Version            : V3.1.0
* Date               : 10/30/2009
* Description        : Virtual Com Port Demo main file
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/
/* Demonstracni program pro 4. dil serialu k STM32F4 Discovery kitu
 * Vyuziti kitu jako USB Virtualniho serioveho portu
 * Upravy ver. 1.0; (C) 2011 Mard, mcu.cz */


/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "usbd_cdc_core.h"
#include "usbd_cdc_vcp.h"
#include "usbd_usr.h"
#include "usbd_desc.h"
#include "stm32f4_discovery.h"
#include <stdio.h>
#include "tools.h"



/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define ADC3_DR_ADDRESS     ((uint32_t)0x4001224C)
#define BufferSize 60000
#define DAC_DHR12R2_ADDRESS    0x40007414
#define DAC_DHR8R1_ADDRESS     0x40007410
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

#define chirp 1
#define chirpSize 1402
const uint16_t Chirp12bit[chirpSize] = { 21, 28, 37, 47, 59, 71, 85, 100, 116, 133, 151, 171, 191, 213, 236, 260, 285, 312, 339, 368, 397, 428, 459, 492, 525, 560, 595, 632, 669, 708, 747, 787, 828, 869, 912, 955, 999, 1044, 1089, 1135, 1181, 1228, 1276, 1324, 1373, 1422, 1472, 1522, 1572, 1623, 1674, 1725, 1777, 1829, 1880, 1932, 1984, 2037, 2089, 2141, 2193, 2245, 2296, 2348, 2399, 2450, 2501, 2551, 2601, 2651, 2700, 2749, 2797, 2845, 2892, 2938, 2984, 3029, 3073, 3116, 3159, 3200, 3241, 3281, 3320, 3358, 3395, 3431, 3465, 3499, 3531, 3562, 3592, 3621, 3649, 3675, 3700, 3723, 3745, 3766, 3785, 3803, 3819, 3834, 3848, 3860, 3870, 3879, 3886, 3892, 3896, 3899, 3899, 3899, 3897, 3893, 3887, 3880, 3871, 3861, 3849, 3836, 3821, 3804, 3785, 3766, 3744, 3721, 3697, 3671, 3643, 3614, 3583, 3551, 3518, 3483, 3447, 3410, 3371, 3331, 3289, 3247, 3203, 3158, 3112, 3065, 3017, 2967, 2917, 2866, 2814, 2761, 2708, 2653, 2598, 2543, 2486, 2429, 2372, 2314, 2256, 2197, 2138, 2079, 2020, 1961, 1901, 1842, 1782, 1723, 1663, 1604, 1545, 1487, 1429, 1371, 1314, 1257, 1201, 1146, 1091, 1037, 984, 932, 881, 830, 781, 733, 686, 640, 595, 552, 510, 469, 430, 392, 356, 321, 288, 256, 227, 199, 172, 148, 125, 104, 85, 67, 52, 39, 27, 18, 10, 5, 1, 0, 0, 3, 7, 14, 23, 33, 46, 61, 78, 96, 117, 140, 165, 191, 220, 250, 283, 317, 353, 390, 430, 471, 514, 558, 604, 652, 701, 751, 803, 856, 910, 966, 1023, 1081, 1139, 1199, 1260, 1322, 1384, 1447, 1511, 1575, 1640, 1705, 1770, 1836, 1902, 1968, 2034, 2101, 2167, 2232, 2298, 2363, 2428, 2492, 2556, 2619, 2682, 2743, 2804, 2864, 2922, 2980, 3037, 3092, 3146, 3198, 3249, 3299, 3347, 3393, 3438, 3481, 3522, 3561, 3599, 3634, 3667, 3699, 3728, 3755, 3780, 3802, 3822, 3840, 3856, 3869, 3880, 3889, 3895, 3898, 3899, 3898, 3894, 3888, 3880, 3869, 3855, 3839, 3820, 3800, 3776, 3751, 3723, 3692, 3660, 3625, 3588, 3549, 3508, 3464, 3419, 3371, 3322, 3271, 3218, 3163, 3107, 3049, 2990, 2929, 2867, 2803, 2739, 2673, 2606, 2538, 2469, 2400, 2330, 2259, 2188, 2116, 2045, 1973, 1900, 1828, 1756, 1684, 1613, 1542, 1471, 1401, 1331, 1263, 1195, 1129, 1063, 999, 935, 874, 813, 754, 697, 642, 588, 537, 487, 439, 394, 350, 309, 270, 234, 200, 168, 139, 113, 89, 68, 50, 35, 22, 12, 5, 1, 0, 1, 6, 13, 23, 36, 52, 71, 93, 117, 144, 174, 207, 242, 280, 321, 364, 410, 457, 508, 560, 615, 672, 730, 791, 854, 918, 984, 1051, 1120, 1191, 1262, 1335, 1408, 1483, 1559, 1635, 1711, 1788, 1866, 1944, 2021, 2099, 2176, 2254, 2330, 2407, 2482, 2557, 2631, 2704, 2775, 2846, 2915, 2982, 3048, 3112, 3175, 3235, 3293, 3350, 3403, 3455, 3504, 3551, 3595, 3636, 3674, 3710, 3743, 3773, 3799, 3823, 3844, 3861, 3875, 3886, 3894, 3898, 3899, 3897, 3892, 3883, 3871, 3855, 3837, 3815, 3789, 3761, 3730, 3695, 3657, 3617, 3573, 3527, 3477, 3425, 3371, 3314, 3254, 3192, 3128, 3062, 2994, 2923, 2851, 2778, 2703, 2626, 2548, 2469, 2389, 2309, 2227, 2145, 2062, 1980, 1897, 1814, 1731, 1649, 1567, 1486, 1405, 1326, 1247, 1170, 1094, 1020, 947, 876, 807, 740, 675, 613, 553, 496, 441, 389, 340, 293, 250, 211, 174, 141, 111, 84, 61, 42, 26, 14, 5, 1, 0, 2, 9, 19, 33, 51, 72, 97, 126, 158, 193, 232, 275, 321, 370, 422, 477, 535, 596, 660, 726, 795, 866, 940, 1015, 1092, 1171, 1252, 1334, 1418, 1502, 1588, 1674, 1761, 1849, 1936, 2024, 2112, 2199, 2286, 2373, 2458, 2543, 2626, 2708, 2789, 2868, 2945, 3020, 3093, 3164, 3232, 3298, 3360, 3420, 3477, 3531, 3582, 3629, 3672, 3713, 3749, 3781, 3810, 3835, 3856, 3873, 3886, 3894, 3899, 3899, 3895, 3887, 3875, 3859, 3839, 3814, 3786, 3753, 3717, 3676, 3632, 3585, 3533, 3478, 3420, 3359, 3294, 3226, 3156, 3083, 3007, 2929, 2849, 2766, 2682, 2596, 2509, 2420, 2330, 2239, 2148, 2056, 1964, 1871, 1779, 1687, 1595, 1505, 1415, 1326, 1239, 1153, 1069, 987, 907, 830, 755, 682, 613, 546, 483, 423, 366, 313, 264, 219, 178, 141, 108, 79, 54, 34, 19, 8, 1, 0, 2, 10, 21, 38, 59, 84, 114, 149, 187, 230, 277, 328, 383, 442, 505, 571, 640, 713, 788, 867, 948, 1031, 1117, 1205, 1295, 1387, 1480, 1574, 1669, 1765, 1861, 1958, 2055, 2152, 2248, 2343, 2438, 2531, 2623, 2714, 2802, 2889, 2973, 3055, 3134, 3210, 3283, 3353, 3419, 3481, 3540, 3595, 3646, 3692, 3734, 3771, 3804, 3832, 3855, 3874, 3887, 3896, 3899, 3898, 3891, 3880, 3863, 3842, 3816, 3784, 3748, 3708, 3662, 3612, 3558, 3500, 3437, 3371, 3300, 3226, 3149, 3069, 2985, 2899, 2810, 2719, 2625, 2530, 2433, 2335, 2236, 2136, 2035, 1934, 1833, 1732, 1632, 1533, 1434, 1337, 1242, 1148, 1057, 968, 881, 797, 717, 640, 566, 496, 430, 368, 310, 257, 209, 165, 126, 92, 63, 40, 22, 9, 2, 0, 3, 12, 26, 46, 71, 101, 137, 178, 224, 274, 330, 390, 454, 523, 596, 673, 753, 837, 925, 1015, 1108, 1203, 1301, 1401, 1502, 1604, 1708, 1813, 1917, 2022, 2127, 2232, 2336, 2438, 2539, 2639, 2737, 2832, 2925, 3015, 3102, 3186, 3266, 3342, 3414, 3482, 3546, 3604, 3658, 3707, 3750, 3788, 3821, 3848, 3870, 3885, 3895, 3899, 3898, 3890, 3876, 3857, 3832, 3801, 3764, 3722, 3675, 3622, 3564, 3501, 3434, 3361, 3285, 3204, 3119, 3031, 2939, 2845, 2747, 2647, 2544, 2440, 2334, 2227, 2119, 2010, 1901, 1792, 1684, 1576, 1470, 1365, 1261, 1160, 1061, 965, 872, 783, 697, 614, 536, 463, 394, 330, 271, 218, 170, 128, 91, 61, 36, 18, 6, 0, 1, 7, 20, 40, 65, 97, 134, 178, 227, 283, 343, 409, 480, 556, 636, 721, 810, 903, 999, 1098, 1201, 1305, 1412, 1521, 1632, 1743, 1855, 1968, 2081, 2193, 2305, 2415, 2524, 2631, 2736, 2838, 2937, 3033, 3126, 3214, 3298, 3378, 3453, 3523, 3587, 3646, 3700, 3747, 3788, 3823, 3851, 3873, 3889, 3897, 3899, 3895, 3883, 3865, 3840, 3809, 3771, 3727, 3676, 3620, 3557, 3489, 3416, 3337, 3254, 3166, 3073, 2977, 2877, 2773, 2667, 2558, 2447, 2334, 2219, 2104, 1988, 1871, 1755, 1640, 1525, 1412, 1301, 1192, 1086, 983, 883, 787, 695, 608, 525, 448, 375, 309, 249, 194, 146, 105, 70, 42, 21, 7, 0, 1, 8, 23, 44, 73, 109, 151, 200, 256, 318, 386, 460, 540, 624, 714, 809, 907, 1010, 1116, 1226, 1338, 1453, 1569, 1687, 1806, 1926, 2046, 2165, 2284, 2402, 2518, 2631, 2743, 2851, 2956, 3057, 3154, 3247, 3334, 3417, 3494, 3564, 3629, 3687, 3739, 3783, 3821, 3852, 3875, 3890, 3898, 3899, 3892, 3877, 3855, 3825, 3788, 3744, 3693, 3635, 3570, 3499, 3422, 3339, 3250, 3156, 3058, 2955, 2847, 2737, 2623, 2506, 2387, 2266, 2144, 2022, 1898, 1775, 1653, 1531, 1411, 1294, 1179, 1067, 958, 853, 753, 658, 567, 483, 404, 331, 265, 206, 154, 109, 72, 42, 20, 6, 0, 2, 12, 30, 55, 89, 130, 179, 235, 299, 369, 446, 529, 618, 713, 813, 917, 1026, 1139, 1256, 1375, 1497, 1621, 1746, 1872, 1999, 2125, 2251, 2376, 2499, 2619, 2737, 2852, 2962, 3069, 3171, 3267, 3358, 3443, 3522, 3594, 3659, 3717, 3767, 3809, 3844, 3870, 3888, 3898, 3899, 3892, 3876, 3852, 3820, 3779, 3731, 3675, 3611, 3540, 3462, 3377, 3286, 3190, 3087, 2980, 2868, 2752, 2632, 2509, 2384, 2257, 2128, 1998, 1868, 1739, 1610, 1483, 1358, 1235, 1115, 1000, 888, 781, 679, 583, 493, 409, 333, 263, 201, 147, 101, 64, 35, 14, 2, 0, 6, 21, 44, 77, 118, 167, 225, 290, 363, 444, 531, 625, 725, 831, 942, 1057, 1177, 1301, 1427, 1556, 1687, 1820, 1953, 2085, 2218, 2349, 2479, 2606, 2730, 2850, 2966, 3078, 3184, 3285, 3379, 3467, 3547, 3620, 3685, 3742, 3791, 3831, 3862, 3883, 3896, 3899, 3894, 3878, 3854, 3821, 3778, 3727, 3667, 3599, 3523, 3439, 3349, 3251, 3147, 3037, 2922, 2802, 2678, 2551, 2420, 2287, 2153, 2017, 1881, 1745, 1610, 1477, 1346, 1217, 1093, 972, 856, 746, 641, 543, 452, 368, 292, 224, 164, 113, 72, 39, 16, 3, 0, 6, 22, 47, 82, 126, 180, 242, 313, 392, 480, 574, 676, 784, 898, 1017, 1141, 1269, 1401, 1536, 1672, 1811, 1950, 2089, 2227, 2364, 2499, 2631, 2760, 2885, 3005, 3120, 3229, 3331, 3426, 3513, 3593, 3664, 3726, 3780, 3823, 3857, 3881, 3895, 3899, 3893, 3877, 3851, 3814, 3768, 3713, 3648, 3574, 3492, 3402, 3304, 3198, 3086, 2968, 2845, 2717, 2585, 2449, 2311, 2170, 2029, 1887, 1745, 1604, 1465, 1329, 1195, 1066, 941, 822, 709, 602, 502, 411, 327, 252, 186, 130, 83, 47, 21, 5};
 
#define sineSize 1019
const uint16_t Sine12bit[sineSize] = { 21, 37, 58, 83, 113, 148, 187, 230, 277, 328, 383, 442, 505, 571, 640, 712, 788, 866, 947, 1030, 1116, 1203, 1293, 1383, 1476, 1569, 1663, 1758, 1854, 1950, 2045, 2141, 2236, 2330, 2423, 2516, 2606, 2696, 2783, 2869, 2952, 3033, 3111, 3187, 3259, 3328, 3394, 3457, 3516, 3571, 3622, 3669, 3712, 3751, 3786, 3816, 3841, 3862, 3878, 3890, 3897, 3900, 3897, 3890, 3878, 3862, 3841, 3816, 3786, 3751, 3712, 3669, 3622, 3571, 3516, 3457, 3394, 3328, 3259, 3187, 3111, 3033, 2952, 2869, 2783, 2696, 2606, 2516, 2423, 2330, 2236, 2141, 2045, 1949, 1854, 1758, 1663, 1569, 1476, 1383, 1293, 1203, 1116, 1030, 947, 866, 788, 712, 640, 571, 505, 442, 383, 328, 277, 230, 187, 148, 113, 83, 58, 37, 21, 9, 2, 0, 2, 9, 21, 37, 58, 83, 113, 148, 187, 230, 277, 328, 383, 442, 505, 571, 640, 712, 788, 866, 947, 1030, 1116, 1203, 1293, 1383, 1476, 1569, 1663, 1758, 1854, 1950, 2045, 2141, 2236, 2330, 2423, 2516, 2606, 2696, 2783, 2869, 2952, 3033, 3111, 3187, 3259, 3328, 3394, 3457, 3516, 3571, 3622, 3669, 3712, 3751, 3786, 3816, 3841, 3862, 3878, 3890, 3897, 3900, 3897, 3890, 3878, 3862, 3841, 3816, 3786, 3751, 3712, 3669, 3622, 3571, 3516, 3457, 3394, 3328, 3259, 3187, 3111, 3033, 2952, 2869, 2783, 2696, 2606, 2516, 2423, 2330, 2236, 2141, 2045, 1949, 1854, 1758, 1663, 1569, 1476, 1383, 1293, 1203, 1116, 1030, 947, 866, 788, 712, 640, 571, 505, 442, 383, 328, 277, 230, 187, 148, 113, 83, 58, 37, 21, 9, 2, 0, 2, 9, 21, 37, 58, 83, 113, 148, 187, 230, 277, 328, 383, 442, 505, 571, 640, 712, 788, 866, 947, 1030, 1116, 1203, 1293, 1383, 1476, 1569, 1663, 1758, 1854, 1950, 2045, 2141, 2236, 2330, 2423, 2516, 2606, 2696, 2783, 2869, 2952, 3033, 3111, 3187, 3259, 3328, 3394, 3457, 3516, 3571, 3622, 3669, 3712, 3751, 3786, 3816, 3841, 3862, 3878, 3890, 3897, 3900, 3897, 3890, 3878, 3862, 3841, 3816, 3786, 3751, 3712, 3669, 3622, 3571, 3516, 3457, 3394, 3328, 3259, 3187, 3111, 3033, 2952, 2869, 2783, 2696, 2606, 2516, 2423, 2330, 2236, 2141, 2045, 1949, 1854, 1758, 1663, 1569, 1476, 1383, 1293, 1203, 1116, 1030, 947, 866, 788, 712, 640, 571, 505, 442, 383, 328, 277, 230, 187, 148, 113, 83, 58, 37, 21, 9, 2, 0, 2, 9, 21, 37, 58, 83, 113, 148, 187, 230, 277, 328, 383, 442, 505, 571, 640, 712, 788, 866, 947, 1030, 1116, 1203, 1293, 1383, 1476, 1569, 1663, 1758, 1854, 1949, 2045, 2141, 2236, 2330, 2423, 2516, 2606, 2696, 2783, 2869, 2952, 3033, 3111, 3187, 3259, 3328, 3394, 3457, 3516, 3571, 3622, 3669, 3712, 3751, 3786, 3816, 3841, 3862, 3878, 3890, 3897, 3900, 3897, 3890, 3878, 3862, 3841, 3816, 3786, 3751, 3712, 3669, 3622, 3571, 3516, 3457, 3394, 3328, 3259, 3187, 3111, 3033, 2952, 2869, 2783, 2696, 2606, 2516, 2423, 2330, 2236, 2141, 2045, 1949, 1854, 1758, 1663, 1569, 1476, 1383, 1293, 1203, 1116, 1030, 947, 866, 788, 712, 640, 571, 505, 442, 383, 328, 277, 230, 187, 148, 113, 83, 58, 37, 21, 9, 2, 0, 2, 9, 21, 37, 58, 83, 113, 148, 187, 230, 277, 328, 383, 442, 505, 571, 640, 712, 788, 866, 947, 1030, 1116, 1203, 1293, 1383, 1476, 1569, 1663, 1758, 1854, 1950, 2045, 2141, 2236, 2330, 2423, 2516, 2606, 2696, 2783, 2869, 2952, 3033, 3111, 3187, 3259, 3328, 3394, 3457, 3516, 3571, 3622, 3669, 3712, 3751, 3786, 3816, 3841, 3862, 3878, 3890, 3897, 3900, 3897, 3890, 3878, 3862, 3841, 3816, 3786, 3751, 3712, 3669, 3622, 3571, 3516, 3457, 3394, 3328, 3259, 3187, 3111, 3033, 2952, 2869, 2783, 2696, 2606, 2516, 2423, 2330, 2236, 2141, 2045, 1949, 1854, 1758, 1663, 1569, 1476, 1383, 1293, 1203, 1116, 1030, 947, 866, 788, 712, 640, 571, 505, 442, 383, 328, 277, 230, 187, 148, 113, 83, 58, 37, 21, 9, 2, 0, 2, 9, 21, 37, 58, 83, 113, 148, 187, 230, 277, 328, 383, 442, 505, 571, 640, 712, 788, 866, 947, 1030, 1116, 1203, 1293, 1383, 1476, 1569, 1663, 1758, 1854, 1949, 2045, 2141, 2236, 2330, 2423, 2516, 2606, 2696, 2783, 2869, 2952, 3033, 3111, 3187, 3259, 3328, 3394, 3457, 3516, 3571, 3622, 3669, 3712, 3751, 3786, 3816, 3841, 3862, 3878, 3890, 3897, 3900, 3897, 3890, 3878, 3862, 3841, 3816, 3786, 3751, 3712, 3669, 3622, 3571, 3516, 3457, 3394, 3328, 3259, 3187, 3111, 3033, 2952, 2869, 2783, 2696, 2606, 2516, 2423, 2330, 2236, 2141, 2045, 1949, 1854, 1758, 1663, 1569, 1476, 1383, 1293, 1203, 1116, 1030, 947, 866, 788, 712, 640, 571, 505, 442, 383, 328, 277, 230, 187, 148, 113, 83, 58, 37, 21, 9, 2, 0, 2, 9, 21, 37, 58, 83, 113, 148, 187, 230, 277, 328, 383, 442, 505, 571, 640, 712, 788, 866, 947, 1030, 1116, 1203, 1293, 1383, 1476, 1569, 1663, 1758, 1854, 1950, 2045, 2141, 2236, 2330, 2423, 2516, 2606, 2696, 2783, 2869, 2952, 3033, 3111, 3187, 3259, 3328, 3394, 3457, 3516, 3571, 3622, 3669, 3712, 3751, 3786, 3816, 3841, 3862, 3878, 3890, 3897, 3900, 3897, 3890, 3878, 3862, 3841, 3816, 3786, 3751, 3712, 3669, 3622, 3571, 3516, 3457, 3394, 3328, 3259, 3187, 3111, 3033, 2952, 2869, 2783, 2696, 2606, 2516, 2423, 2330, 2236, 2141, 2045, 1949, 1854, 1758, 1663, 1569, 1476, 1383, 1293, 1203, 1116, 1030, 947, 866, 788, 712, 640, 571, 505, 442, 383, 328, 277, 230, 187, 148, 113, 83, 58, 37, 21, 9, 2, 0, 2, 9, 21, 37, 58, 83, 113, 148, 187, 230, 277, 328, 383, 442, 505, 571, 640, 712, 788, 866, 947, 1030, 1116, 1203, 1293, 1383, 1476, 1569, 1663, 1758, 1854, 1950, 2045, 2141, 2236, 2330, 2423, 2516, 2606, 2696, 2783, 2869, 2952, 3033, 3111, 3187, 3259, 3328, 3394, 3457, 3516, 3571, 3622, 3669, 3712, 3751, 3786, 3816, 3841, 3862, 3878, 3890, 3897, 3900, 3897, 3890, 3878, 3862, 3841, 3816, 3786, 3751, 3712, 3669, 3622, 3571, 3516, 3457, 3394, 3328, 3259, 3187, 3111, 3033, 2952, 2869, 2783, 2696, 2606, 2516, 2423, 2330, 2236, 2141, 2045, 1949, 1854, 1758, 1663, 1569, 1476, 1383, 1293, 1203, 1116, 1030, 947, 866, 788, 712, 640, 571, 505, 442, 383, 328, 277, 230, 187, 148, 113, 83, 58, 37, 21};
 
const uint8_t Sine8bit[1019] = { 1, 2, 3, 5, 7, 9, 12, 14, 18, 21, 24, 28, 32, 37, 41, 46, 51, 56, 61, 67, 72, 78, 84, 90, 96, 102, 108, 114, 120, 127, 133, 139, 145, 151, 157, 163, 169, 175, 181, 186, 192, 197, 202, 207, 212, 216, 221, 225, 229, 232, 235, 239, 241, 244, 246, 248, 250, 251, 252, 253, 253, 254, 253, 253, 252, 251, 250, 248, 246, 244, 241, 239, 235, 232, 229, 225, 221, 216, 212, 207, 202, 197, 192, 186, 181, 175, 169, 163, 157, 151, 145, 139, 133, 126, 120, 114, 108, 102, 96, 90, 84, 78, 72, 67, 61, 56, 51, 46, 41, 37, 32, 28, 24, 21, 18, 14, 12, 9, 7, 5, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 5, 7, 9, 12, 14, 18, 21, 24, 28, 32, 37, 41, 46, 51, 56, 61, 67, 72, 78, 84, 90, 96, 102, 108, 114, 120, 127, 133, 139, 145, 151, 157, 163, 169, 175, 181, 186, 192, 197, 202, 207, 212, 216, 221, 225, 229, 232, 235, 239, 241, 244, 246, 248, 250, 251, 252, 253, 253, 254, 253, 253, 252, 251, 250, 248, 246, 244, 241, 239, 235, 232, 229, 225, 221, 216, 212, 207, 202, 197, 192, 186, 181, 175, 169, 163, 157, 151, 145, 139, 133, 126, 120, 114, 108, 102, 96, 90, 84, 78, 72, 67, 61, 56, 51, 46, 41, 37, 32, 28, 24, 21, 18, 14, 12, 9, 7, 5, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 5, 7, 9, 12, 14, 18, 21, 24, 28, 32, 37, 41, 46, 51, 56, 61, 67, 72, 78, 84, 90, 96, 102, 108, 114, 120, 127, 133, 139, 145, 151, 157, 163, 169, 175, 181, 186, 192, 197, 202, 207, 212, 216, 221, 225, 229, 232, 235, 239, 241, 244, 246, 248, 250, 251, 252, 253, 253, 254, 253, 253, 252, 251, 250, 248, 246, 244, 241, 239, 235, 232, 229, 225, 221, 216, 212, 207, 202, 197, 192, 186, 181, 175, 169, 163, 157, 151, 145, 139, 133, 126, 120, 114, 108, 102, 96, 90, 84, 78, 72, 67, 61, 56, 51, 46, 41, 37, 32, 28, 24, 21, 18, 14, 12, 9, 7, 5, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 5, 7, 9, 12, 14, 18, 21, 24, 28, 32, 37, 41, 46, 51, 56, 61, 67, 72, 78, 84, 90, 96, 102, 108, 114, 120, 126, 133, 139, 145, 151, 157, 163, 169, 175, 181, 186, 192, 197, 202, 207, 212, 216, 221, 225, 229, 232, 235, 239, 241, 244, 246, 248, 250, 251, 252, 253, 253, 254, 253, 253, 252, 251, 250, 248, 246, 244, 241, 239, 235, 232, 229, 225, 221, 216, 212, 207, 202, 197, 192, 186, 181, 175, 169, 163, 157, 151, 145, 139, 133, 126, 120, 114, 108, 102, 96, 90, 84, 78, 72, 67, 61, 56, 51, 46, 41, 37, 32, 28, 24, 21, 18, 14, 12, 9, 7, 5, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 5, 7, 9, 12, 14, 18, 21, 24, 28, 32, 37, 41, 46, 51, 56, 61, 67, 72, 78, 84, 90, 96, 102, 108, 114, 120, 127, 133, 139, 145, 151, 157, 163, 169, 175, 181, 186, 192, 197, 202, 207, 212, 216, 221, 225, 229, 232, 235, 239, 241, 244, 246, 248, 250, 251, 252, 253, 253, 254, 253, 253, 252, 251, 250, 248, 246, 244, 241, 239, 235, 232, 229, 225, 221, 216, 212, 207, 202, 197, 192, 186, 181, 175, 169, 163, 157, 151, 145, 139, 133, 126, 120, 114, 108, 102, 96, 90, 84, 78, 72, 67, 61, 56, 51, 46, 41, 37, 32, 28, 24, 21, 18, 14, 12, 9, 7, 5, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 5, 7, 9, 12, 14, 18, 21, 24, 28, 32, 37, 41, 46, 51, 56, 61, 67, 72, 78, 84, 90, 96, 102, 108, 114, 120, 126, 133, 139, 145, 151, 157, 163, 169, 175, 181, 186, 192, 197, 202, 207, 212, 216, 221, 225, 229, 232, 235, 239, 241, 244, 246, 248, 250, 251, 252, 253, 253, 254, 253, 253, 252, 251, 250, 248, 246, 244, 241, 239, 235, 232, 229, 225, 221, 216, 212, 207, 202, 197, 192, 186, 181, 175, 169, 163, 157, 151, 145, 139, 133, 126, 120, 114, 108, 102, 96, 90, 84, 78, 72, 67, 61, 56, 51, 46, 41, 37, 32, 28, 24, 21, 18, 14, 12, 9, 7, 5, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 5, 7, 9, 12, 14, 18, 21, 24, 28, 32, 37, 41, 46, 51, 56, 61, 67, 72, 78, 84, 90, 96, 102, 108, 114, 120, 127, 133, 139, 145, 151, 157, 163, 169, 175, 181, 186, 192, 197, 202, 207, 212, 216, 221, 225, 229, 232, 235, 239, 241, 244, 246, 248, 250, 251, 252, 253, 253, 254, 253, 253, 252, 251, 250, 248, 246, 244, 241, 239, 235, 232, 229, 225, 221, 216, 212, 207, 202, 197, 192, 186, 181, 175, 169, 163, 157, 151, 145, 139, 133, 126, 120, 114, 108, 102, 96, 90, 84, 78, 72, 67, 61, 56, 51, 46, 41, 37, 32, 28, 24, 21, 18, 14, 12, 9, 7, 5, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 5, 7, 9, 12, 14, 18, 21, 24, 28, 32, 37, 41, 46, 51, 56, 61, 67, 72, 78, 84, 90, 96, 102, 108, 114, 120, 127, 133, 139, 145, 151, 157, 163, 169, 175, 181, 186, 192, 197, 202, 207, 212, 216, 221, 225, 229, 232, 235, 239, 241, 244, 246, 248, 250, 251, 252, 253, 253, 254, 253, 253, 252, 251, 250, 248, 246, 244, 241, 239, 235, 232, 229, 225, 221, 216, 212, 207, 202, 197, 192, 186, 181, 175, 169, 163, 157, 151, 145, 139, 133, 126, 120, 114, 108, 102, 96, 90, 84, 78, 72, 67, 61, 56, 51, 46, 41, 37, 32, 28, 24, 21, 18, 14, 12, 9, 7, 5, 3, 2, 1};


 

__ALIGN_BEGIN USB_OTG_CORE_HANDLE    USB_OTG_dev __ALIGN_END ;

TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure1;
TIM_OCInitTypeDef  TIM_OCInitStructure1;
TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure2;
TIM_OCInitTypeDef  TIM_OCInitStructure2;
DAC_InitTypeDef  DAC_InitStructure1;
DAC_InitTypeDef  DAC_InitStructure2;
DMA_InitTypeDef DMA_InitStructure;
DMA_InitTypeDef DMA_InitStructure1;
GPIO_InitTypeDef GPIO_InitStructure_Dac;
ADC_InitTypeDef       ADC_InitStructure;
ADC_CommonInitTypeDef ADC_CommonInitStructure;
DMA_InitTypeDef       DMA_ADC_InitStructure;
GPIO_InitTypeDef      GPIO_ADC_InitStructure;
NVIC_InitTypeDef      NVIC_ADC_InitStructure;

uint16_t TIM_Period1 = 698;//698->40,05kHz
#ifdef chirp
uint16_t TIM6_Period = 8;
#else
uint16_t TIM6_Period = 8;
#endif
uint16_t TIM_Period2 = 25;//25->550kHz
uint16_t PrescalerValue = 0;
uint8_t send = 0;

/* You can monitor the converted value by adding the variable "ADC3ConvertedValue" 
   to the debugger watch window */
__IO uint16_t ADC3ConvertedValue[BufferSize];


/* Extern variables ----------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

void TIM_Config(void);
void ADC3_Config(void);
void DMA2_Stream0_IRQHandler(void);
void Tim_switch(int8_t on_off, int8_t chanel);
void TIM6_Config(void);
void DAC_Config(void);
void DAC_Trig(void);
void ADC3_Trig(void);
void PWM_Trig(uint32_t nTick);
/* Private functions ---------------------------------------------------------*/
/*******************************************************************************
* Function Name  : main.
* Description    : Main routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
int main(void)
{	 
	uint16_t i = 0;
	    
	STM32F4_Discovery_LEDInit(LED3);
	STM32F4_Discovery_LEDInit(LED4);
	STM32F4_Discovery_LEDInit(LED5);
	STM32F4_Discovery_LEDInit(LED6);

  USBD_Init(&USB_OTG_dev,
#ifdef USE_USB_OTG_HS 
  USB_OTG_HS_CORE_ID,
#else            
  USB_OTG_FS_CORE_ID,
#endif  
  &USR_desc, 
  &USBD_CDC_cb,
  &USR_cb);

	TIM_Config();	
	TIM6_Config(); 
	
	DAC_Config();
	ADC3_Config();
	
  while (1)
  {					
		STM32F4_Discovery_LEDOn(LED5);  
		
		ADC3_Trig();
		Delay(30*TIM_Period1);	
					
		DAC_Trig();
		PWM_Trig(8*TIM_Period1);
		
		//Delay(200*TIM_Period1);	//time to cooldown for recivers don't see transmision 
		//ADC3_Trig();
		
		while (send == 0);
		STM32F4_Discovery_LEDOff(LED5);
		STM32F4_Discovery_LEDToggle(LED4);
		
		STM32F4_Discovery_LEDOn(LED6);
		printf("**** start ****\n");
		for (i=0;i<BufferSize;i+=4)
		{
			printf("%.3x;%.3x;%.3x;%.3x\n", ADC3ConvertedValue[i+0], ADC3ConvertedValue[i+1], ADC3ConvertedValue[i+2], ADC3ConvertedValue[i+3]);	
			Delay(10*168);
		}
		printf("**** stop  ****\n");	
		Delay(100*168);
		STM32F4_Discovery_LEDOff(LED6);
		send = 0;		
  }
}

/**
  * @brief  
  * @param 
  * @retval : 
  */


void DMA2_Stream0_IRQHandler(void)
{
	DMA_ClearITPendingBit(DMA2_Stream0, DMA_FLAG_TCIF0);
	send = 1;
	STM32F4_Discovery_LEDToggle(LED3); 	
}

void DAC_Trig(void){
  /* DMA1_Stream6 channel7 configuration **************************************/  
  DMA_DeInit(DMA1_Stream6);
  DMA_InitStructure1.DMA_Channel = DMA_Channel_7;  
  DMA_InitStructure1.DMA_PeripheralBaseAddr = DAC_DHR8R1_ADDRESS;
  DMA_InitStructure1.DMA_Memory0BaseAddr = (uint32_t)&Sine8bit;
  DMA_InitStructure1.DMA_BufferSize = sineSize;
  DMA_InitStructure1.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  DMA_InitStructure1.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure1.DMA_DIR = DMA_DIR_MemoryToPeripheral;
  DMA_InitStructure1.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure1.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure1.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure1.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure1.DMA_FIFOMode = DMA_FIFOMode_Disable;         
  DMA_InitStructure1.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
  DMA_InitStructure1.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  DMA_InitStructure1.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_Init(DMA1_Stream6, &DMA_InitStructure1);    

  /* Enable DMA1_Stream6 */
  DMA_Cmd(DMA1_Stream6, ENABLE);
  
  /* Enable DAC Channel1 */
  DAC_Cmd(DAC_Channel_1, ENABLE);

  /* Enable DMA for DAC Channel1 */
  DAC_DMACmd(DAC_Channel_1, ENABLE);  

  /* DMA1_Stream5 channel7 configuration **************************************/
  DMA_DeInit(DMA1_Stream5);
  DMA_InitStructure.DMA_Channel = DMA_Channel_7;  
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)DAC_DHR12R2_ADDRESS;  
  DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	
	#ifdef chirp
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&Chirp12bit;
	DMA_InitStructure.DMA_BufferSize = chirpSize;
	#else
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&Sine12bit;
	DMA_InitStructure.DMA_BufferSize = sineSize;
	#endif
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_Init(DMA1_Stream5, &DMA_InitStructure);

  /* Enable DMA1_Stream5 */
  DMA_Cmd(DMA1_Stream5, ENABLE);

  /* Enable DAC Channel2 */
  DAC_Cmd(DAC_Channel_2, ENABLE);

  /* Enable DMA for DAC Channel2 */
  DAC_DMACmd(DAC_Channel_2, ENABLE);
}

void DAC_Config(void)
{	
  /* DMA1 clock and GPIOA clock enable (to be used with DAC) */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1 | RCC_AHB1Periph_GPIOA, ENABLE);

  /* DAC Periph clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);

  /* DAC channel 2 (DAC_OUT2 = PA.5) configuration */
  GPIO_InitStructure_Dac.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
  GPIO_InitStructure_Dac.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure_Dac.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStructure_Dac);
	
	DAC_DeInit();	  
	
	/* DAC channel1 Configuration */
  DAC_InitStructure1.DAC_Trigger = DAC_Trigger_T6_TRGO;
  DAC_InitStructure1.DAC_WaveGeneration = DAC_WaveGeneration_None;
  DAC_InitStructure1.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
  DAC_Init(DAC_Channel_1, &DAC_InitStructure1);
	
	/* DAC channel2 Configuration */
  DAC_InitStructure2.DAC_Trigger = DAC_Trigger_T6_TRGO;
  DAC_InitStructure2.DAC_WaveGeneration = DAC_WaveGeneration_None;
  DAC_InitStructure2.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
  DAC_Init(DAC_Channel_2, &DAC_InitStructure2);
}

void ADC3_Trig() {
	/* DMA2 Stream0 channel0 configuration **************************************/
	DMA_DeInit(DMA2_Stream0);
  DMA_InitStructure.DMA_Channel = DMA_Channel_2;
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)ADC3_DR_ADDRESS;
  DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&ADC3ConvertedValue[0];
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
  DMA_InitStructure.DMA_BufferSize = BufferSize;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; 
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable; 
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_Init(DMA2_Stream0, &DMA_InitStructure);
	DMA_ITConfig(DMA2_Stream0, DMA_IT_TC, ENABLE);
  DMA_Cmd(DMA2_Stream0, ENABLE);
	
	ADC_DeInit();
  /* ADC Common Init **********************************************************/
  ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
  ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
  ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled; // Orig dis
  ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_10Cycles;
  ADC_CommonInit(&ADC_CommonInitStructure);

  /* ADC3 Init ****************************************************************/
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStructure.ADC_ScanConvMode = ENABLE; //orig disable
  ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_Rising;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T2_CC3;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_NbrOfConversion = 4;
  ADC_Init(ADC3, &ADC_InitStructure);

  /* ADC3 regular channel12 configuration *************************************/
  ADC_RegularChannelConfig(ADC3, ADC_Channel_0, 1, ADC_SampleTime_3Cycles);
  ADC_RegularChannelConfig(ADC3, ADC_Channel_1, 2, ADC_SampleTime_3Cycles);
  ADC_RegularChannelConfig(ADC3, ADC_Channel_2, 3, ADC_SampleTime_3Cycles);
  ADC_RegularChannelConfig(ADC3, ADC_Channel_3, 4, ADC_SampleTime_3Cycles);

	/* Enable DMA request after last transfer (Single-ADC mode) */
	ADC_DMARequestAfterLastTransferCmd(ADC3, ENABLE);

  /* Enable ADC3 DMA */
  ADC_DMACmd(ADC3, ENABLE);

  /* Enable ADC3 */
  ADC_Cmd(ADC3, ENABLE);
}

/**
  * @brief  ADC3 channel12 with DMA configuration
  * @param  None
  * @retval None
  */
void ADC3_Config(void)
{
  /* Enable ADC3, DMA2 and GPIO clocks ****************************************/
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2 | RCC_AHB1Periph_GPIOA, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC3, ENABLE);

  /* Configure and enable DMA interrupt */
  NVIC_ADC_InitStructure.NVIC_IRQChannel = DMA2_Stream0_IRQn;
  NVIC_ADC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_ADC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_ADC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_ADC_InitStructure);
	NVIC_EnableIRQ(DMA2_Stream0_IRQn);

  /* Configure ADC3 Channel12 pin as analog input ******************************/
  GPIO_ADC_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3;
  GPIO_ADC_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_ADC_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_Init(GPIOA, &GPIO_ADC_InitStructure);
}
	

/**
  * @brief  Configure the TIM3 Ouput Channels.
  * @param  None
  * @retval None
  */
void TIM_Config(void)
{
  GPIO_InitTypeDef GPIO_InitStructure1;
  GPIO_InitTypeDef GPIO_InitStructure2;

  /* TIM3 clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 | RCC_APB1Periph_TIM2, ENABLE);

  /* GPIOC and GPIOB clock enable */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOB, ENABLE);
  
  /* GPIOC Configuration: TIM3 CH1 (PC6) */
  GPIO_InitStructure1.GPIO_Pin = GPIO_Pin_6;
  GPIO_InitStructure1.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure1.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure1.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure1.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_Init(GPIOC, &GPIO_InitStructure1); 
	
	GPIO_InitStructure2.GPIO_Pin = GPIO_Pin_10;
  GPIO_InitStructure2.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure2.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure2.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure2.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_Init(GPIOB, &GPIO_InitStructure2); 
  

  /* Connect TIM3 pins to AF2 */  
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_TIM3);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_TIM2);
	

  /* Compute the prescaler value */
  PrescalerValue = (uint16_t) ((SystemCoreClock /2) / 28000000) - 1;

  /* Time base configuration */
  TIM_TimeBaseStructure1.TIM_Period = TIM_Period1;
	TIM_TimeBaseStructure1.TIM_Prescaler = PrescalerValue;
  TIM_TimeBaseStructure1.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure1.TIM_CounterMode = TIM_CounterMode_Up;

  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure1);

  /* PWM1 Mode configuration: Channel1 */
  TIM_OCInitStructure1.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure1.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure1.TIM_Pulse = 0;
  TIM_OCInitStructure1.TIM_OCPolarity = TIM_OCPolarity_High;
	
	TIM_OC1Init(TIM3, &TIM_OCInitStructure1);
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM3, ENABLE);
	
	/* TIM3 enable counter */
  TIM_Cmd(TIM3, ENABLE);
	
	/* PWM1 Mode configuration: Channel2 */
	TIM_TimeBaseStructure2.TIM_Period = TIM_Period2;
	TIM_TimeBaseStructure2.TIM_Prescaler = PrescalerValue;
  TIM_TimeBaseStructure2.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure2.TIM_CounterMode = TIM_CounterMode_Up;
	
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure2);
	
	TIM_OCInitStructure2.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure2.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure2.TIM_Pulse = TIM_Period2/2;
  TIM_OCInitStructure2.TIM_OCPolarity = TIM_OCPolarity_High;

  TIM_OC3Init(TIM2, &TIM_OCInitStructure2);
  TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);	
  TIM_ARRPreloadConfig(TIM2, ENABLE);
  
  TIM_Cmd(TIM2, ENABLE);
}

/**
  * @brief  TIM6 Configuration
  * @note   TIM6 configuration is based on CPU @168MHz and APB1 @42MHz
  * @note   TIM6 Update event occurs each 37.5MHz/256 = 16.406 KHz    
  * @param  None
  * @retval None
  */
void TIM6_Config(void)
{
  TIM_TimeBaseInitTypeDef    TIM_TimeBaseStructure;
  /* TIM6 Periph clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);

  /* Time base configuration */
  TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
  TIM_TimeBaseStructure.TIM_Period = TIM6_Period;
  TIM_TimeBaseStructure.TIM_Prescaler = 0;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
  TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);

  /* TIM6 TRGO selection */
  TIM_SelectOutputTrigger(TIM6, TIM_TRGOSource_Update);
  
  /* TIM6 enable counter */
  TIM_Cmd(TIM6, ENABLE);
}

void PWM_Trig(uint32_t nTick){
	TIM3->CCR1 = TIM_Period1/2;
	Delay(nTick);
	TIM3->CCR1 = 0;
}

void Tim_switch(int8_t on_off, int8_t chanel)
{
	if (on_off == 0)
	{
		if (chanel == 1) 
		{
			TIM3->CCR1 = 0;
		}
		if (chanel == 2) 
		{
			TIM2->CCR3 = 0;
		}
	} else 
	{
		if (chanel == 1) 
		{
			TIM3->CCR1 = TIM_Period1/2;
		}
		if (chanel == 2) 
		{
			TIM2->CCR3 = TIM_Period2/2;
		}
	}
}

#ifdef USE_FULL_ASSERT
/*******************************************************************************
* Function Name  : assert_failed
* Description    : Reports the name of the source file and the source line number
*                  where the assert_param error has occurred.
* Input          : - file: pointer to the source file name
*                  - line: assert_param error line source number
* Output         : None
* Return         : None
*******************************************************************************/
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {}
}
#endif

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
