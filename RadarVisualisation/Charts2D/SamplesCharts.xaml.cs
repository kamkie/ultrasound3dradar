﻿using System.Windows.Controls;
using UltrasoundMonitor;

namespace RadarVisualisation.Charts2D {
    /// <summary>
    /// Interaction logic for SamplesCharts.xaml
    /// </summary>
    public partial class SamplesCharts : UserControl
    {
        public SamplesCharts()
        {
            InitializeComponent();
        }

        public void refreshCharts(DataFlow dataFlow)
        {
            samplesDataModel.Samples = dataFlow.dataFrame;

            plotSamples.InvalidatePlot(true);
        }

        public void refreshSettings()
        {
            samplesDataModel.refreshSettings();
        }
    }
}
