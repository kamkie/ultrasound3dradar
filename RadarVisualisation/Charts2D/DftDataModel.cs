﻿namespace RadarVisualisation.Charts2D {
    using System.ComponentModel;
    using OxyPlot;
    using OxyPlot.Axes;
    using OxyPlot.Series;
    using UltrasoundMonitor;


    /// <summary>
    /// Represents the view-model for the main window.
    /// </summary>
    class DftDataModel : INotifyPropertyChanged {
        private double[,,] stftMag;
        private double[,] averageBandpassDft;
        private int[,] pointsInSpace;
        private double xAxisScale;
        private double yAxisScale;
        private uint NumberOfChanels;
        public DftDataModel() {
            refreshSettings();
            setupStftData();
            setupAvgDftData();
            setupPisData();
        }

        public void refreshSettings() {
            xAxisScale = 1000.0 * (DataFlow.settings.DftLength - DataFlow.settings.OverlapWindow) / (double)DataFlow.settings.SamplingRate;
            yAxisScale = DataFlow.settings.SamplingRate / (1000.0 * DataFlow.settings.DftLength);
            NumberOfChanels = DataFlow.settings.NumberOfChanels;
        }

        private void setupStftData() {
            this.StftModel = new PlotModel[DataFlow.settings.NumberOfChanels];

            for (int i = 0; i < DataFlow.settings.NumberOfChanels; i++) {
                StftModel[i] = new PlotModel() { Title = "Stft plot", Subtitle = "Chanel " + (i + 1) };
                StftModel[i].Axes.Add(new LinearColorAxis { Position = AxisPosition.Right, Palette = OxyPalettes.Jet(500), HighColor = OxyColors.Gray, LowColor = OxyColors.Black });
            }
        }

        private void setupAvgDftData() {
            this.AvgDftModel = new PlotModel() { Title = "Avg Dft plot", Subtitle = "4 chanels" };
            LineSeries[] chanels = new LineSeries[DataFlow.settings.NumberOfChanels];
            for (int i = 0; i < chanels.Length; i++) {
                chanels[i] = new LineSeries() { Title = "Chanel " + (int)(i + 1), MarkerType = MarkerType.Diamond };
                this.AvgDftModel.Series.Add(chanels[i]);
            }
        }
        private void setupPisData() {
            this.PisModel = new PlotModel() { Title = "Points in space plot", Subtitle = "4 chanels" };
            PisModel.Axes.Add(new LinearColorAxis { Position = AxisPosition.Right, Palette = OxyPalettes.Jet(500), HighColor = OxyColors.Gray, LowColor = OxyColors.Black });
        }

        private void updateStftData() {
            for (int i = 0; i < NumberOfChanels; i++) {
                double[,] data = new double[stftMag.GetLength(2), stftMag.GetLength(1)];
                for (int j = 0; j < stftMag.GetLength(1); j++) {
                    for (int k = 0; k < stftMag.GetLength(2); k++) {
                        data[k, j] = stftMag[i, j, k];
                    }
                }
                HeatMapSeries hms = new HeatMapSeries { X0 = 0, X1 = stftMag.GetLength(2) * xAxisScale, Y0 = stftMag.GetLength(1) * yAxisScale, Y1 = 0, Data = data, Interpolate = false };
                if (StftModel[i].Axes.Count > 2) {
                    StftModel[i].Axes[1].Unit = "ms";
                    StftModel[i].Axes[2].Unit = "kHz";
                }
                StftModel[i].Series.Clear();
                this.StftModel[i].Series.Add(hms);
            }

            RaisePropertyChanged("StftModel");
        }

        private void updateBandSpectrumData() {
            for (int k = 0; k < averageBandpassDft.GetLength(0); k++) {
                LineSeries s = AvgDftModel.Series[k] as LineSeries;
                if (s == null) continue;
                s.Points.Clear();
                for (int i = 0; i < averageBandpassDft.GetLength(1); i++) {
                    s.Points.Add(new DataPoint(i * xAxisScale, averageBandpassDft[k, i]));
                }
                if (s.XAxis != null)
                    s.XAxis.Unit = "ms";
            }

            RaisePropertyChanged("AvgDftModel");
        }

        private void updatePisData() {
            double[,] data = new double[pointsInSpace.GetLength(1), pointsInSpace.GetLength(0)];

            for (int k = 0; k < pointsInSpace.GetLength(0); k++) {
                for (int i = 0; i < pointsInSpace.GetLength(1); i++) {
                    data[i, k] = pointsInSpace[k, i];
                }
            }

            HeatMapSeries hms = new HeatMapSeries { X0 = 0, X1 = pointsInSpace.GetLength(1) * xAxisScale, Y0 = pointsInSpace.GetLength(0), Y1 = 0, Data = data, Interpolate = false };

            this.PisModel.Series.Clear();
            PisModel.Series.Add(hms);
            if (PisModel.Axes.Count > 2) {
                PisModel.Axes[1].Unit = "ms";
                PisModel.Axes[2].Unit = "chanel";
            }

            RaisePropertyChanged("PisModel");
        }

        /// <summary>
        /// Gets the plot model.
        /// </summary>
        public PlotModel[] StftModel { get; set; }

        public PlotModel AvgDftModel { get; set; }

        public PlotModel PisModel { get; set; }

        public double[,,] StftData {
            set {
                stftMag = value;
                updateStftData();
            }
            get {
                return stftMag;
            }
        }

        public double[,] AverageBandpassDft {
            set {
                averageBandpassDft = value;
                updateBandSpectrumData();
            }
            get {
                return averageBandpassDft;
            }
        }

        public int[,] PointsInSpace {
            set {
                pointsInSpace = value;
                updatePisData();
            }
            get {
                return pointsInSpace;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string property) {
            var handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(property));
        }

    }

}
