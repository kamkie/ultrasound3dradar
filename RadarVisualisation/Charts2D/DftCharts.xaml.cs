﻿using System.Windows.Controls;
using UltrasoundMonitor;

namespace RadarVisualisation.Charts2D {
    /// <summary>
    /// Interaction logic for DftCharts.xaml
    /// </summary>
    public partial class DftCharts : UserControl
    {
        public DftCharts()
        {
            InitializeComponent();
        }

        public void refreshCharts(DataFlow dataFlow)
        {
            dftDataModel.StftData = dataFlow.stftMag;
            dftDataModel.PointsInSpace = dataFlow.pointsInSpace;
            dftDataModel.AverageBandpassDft = dataFlow.averageBandpassDft;

            plotStftCh1.InvalidatePlot(true);
            plotStftCh2.InvalidatePlot(true);
            plotStftCh3.InvalidatePlot(true);
            plotStftCh4.InvalidatePlot(true);
            plotPis.InvalidatePlot(true);
            plotAvgDft.InvalidatePlot(true);
        }

        public void refreshSettings()
        {
            dftDataModel.refreshSettings();
        }
    }
}
