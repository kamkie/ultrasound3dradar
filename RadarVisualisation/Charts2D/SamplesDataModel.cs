﻿namespace RadarVisualisation.Charts2D
{
    using System.ComponentModel;
    using OxyPlot;
    using OxyPlot.Series;
    using UltrasoundMonitor;


    /// <summary>
    /// Represents the view-model for the main window.
    /// </summary>
    class SamplesDataModel : INotifyPropertyChanged
    {
        private double[][] samples;
        private double xAxisScale = 1000 / (double) DataFlow.settings.SamplingRate;

        public SamplesDataModel()
        {
            setupSamplesData();
        }

        public void refreshSettings()
        {
            xAxisScale = 1000 / (double) DataFlow.settings.SamplingRate;
        }

        private void setupSamplesData()
        {
            this.SamplesModel = new PlotModel() {Title = "samples", Subtitle = "4 chanels"};
            LineSeries[] chanels = new LineSeries[DataFlow.settings.NumberOfChanels];
            for (int i = 0; i < chanels.Length; i++)
            {
                chanels[i] = new LineSeries() {Title = "Chanel " + (i + 1), MarkerType = MarkerType.Diamond};
                this.SamplesModel.Series.Add(chanels[i]);
            }
        }


        private void updateSamplesData()
        {
            for (int k = 0; k < SamplesModel.Series.Count; k++)
            {
                LineSeries s = SamplesModel.Series[k] as LineSeries;
                if (s == null) continue;
                s.Points.Clear();

                for (int i = 0; i < samples.Length; i++)
                {
                    s.Points.Add(new DataPoint(i * xAxisScale, samples[i][k] * 3.3 / 4096.0));
                }
                if (s.XAxis != null)
                    s.XAxis.Unit = "ms";
                if (s.YAxis != null)
                    s.YAxis.Unit = "V";
            }

            RaisePropertyChanged("SamplesModel");
        }


        public PlotModel SamplesModel { get; set; }


        public double[][] Samples
        {
            set
            {
                samples = value;
                updateSamplesData();
            }
            get { return samples; }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string property)
        {
            var handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}