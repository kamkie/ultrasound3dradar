﻿using System;

namespace RadarVisualisation.Charts3D
{
    class SphericalPoint
    {
        public double Radius { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }

        public SphericalPoint(double radius, double longitude, double latitude)
        {
            if (radius < 0)
            {
                throw new Exception("Radius has to be greater than 0");
            }

            if (longitude < 0 || longitude > 2 * Math.PI)
            {
                throw new Exception("Longitude should be in range <0; 2pi>");
            }

            if (latitude < (-1 * 0.5 * Math.PI) || latitude > (0.5 * Math.PI))
            {
                throw new Exception("Latitude should be in range <-0.5pi; 0.5pi>");
            }

            this.Radius = radius;
            this.Latitude = latitude;
            this.Longitude = longitude;
        }

        public double X => Radius * Math.Cos(Latitude) * Math.Cos(Longitude);

        public double Y => Radius * Math.Cos(Latitude) * Math.Sin(Longitude);

        public double Z => Radius * Math.Sin(Latitude);
    }
}