﻿using System.Linq;

namespace RadarVisualisation.Charts3D
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using System.Windows.Media.Media3D;
    using HelixToolkit.Wpf;

    /// <summary>
    /// Provides a ViewModel for the Main window.
    /// </summary>
    public class PointsDataModel : INotifyPropertyChanged
    {
        private List<double[]> pointsSphericalCoordinates;
        private MeshBuilder meshBuilderNew;
        private MeshBuilder meshBuilderNormal;
        private MeshBuilder meshBuilderDeleted;
        private Material deleteMaterial;
        private Material normalMaterial;
        private Material newMaterial;
        private Material insideMaterial;
        private Point3D[] oldPoints = new Point3D[1];
        private ConcurrentQueue<Point3D> currentPoints;
        private ConcurrentQueue<Point3D> newPoints;
        private ConcurrentQueue<Point3D> normalPoints;
        private double DeletedPointSize = MainWindow.settings.DeletedPointSize;
        private double NormalPointSize = MainWindow.settings.NormalPointSize;
        private double NewPointSize = MainWindow.settings.NewPointSize;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public PointsDataModel()
        {
            SetupPointsData();
        }

        public void RefreshSettings()
        {
            DeletedPointSize = MainWindow.settings.DeletedPointSize;
            NormalPointSize = MainWindow.settings.NormalPointSize;
            NewPointSize = MainWindow.settings.NewPointSize;
        }

        public PointsDataModel(List<double[]> pointsSphericalCoordinates)
        {
            SetupPointsData();
            this.pointsSphericalCoordinates = pointsSphericalCoordinates;
        }

        private void SetupPointsData()
        {
            SphericalPointModel = new Model3DGroup();
            // Create some materials
            deleteMaterial = MaterialHelper.CreateMaterial(Colors.Red);
            normalMaterial = MaterialHelper.CreateMaterial(Colors.Blue);
            newMaterial = MaterialHelper.CreateMaterial(Colors.Green);
            insideMaterial = MaterialHelper.CreateMaterial(Colors.Yellow);
        }

        public void UpdatePointsData()
        {
            meshBuilderNew = new MeshBuilder(false, false);
            meshBuilderNormal = new MeshBuilder(false, false);
            meshBuilderDeleted = new MeshBuilder(false, false);
            currentPoints = new ConcurrentQueue<Point3D>();
            newPoints = new ConcurrentQueue<Point3D>();
            normalPoints = new ConcurrentQueue<Point3D>();
            Parallel.For(0, pointsSphericalCoordinates.Count, i =>
            {
                if (pointsSphericalCoordinates[i] == null) return;

                var sPoint = new SphericalPoint(pointsSphericalCoordinates[i][0], pointsSphericalCoordinates[i][1],
                    pointsSphericalCoordinates[i][2]);
                var point = new Point3D(sPoint.X, sPoint.Y, sPoint.Z);
                var isOld = oldPoints.Any(point3D => point.DistanceTo(point3D) < NormalPointSize);
                currentPoints.Enqueue(point);
                if (isOld)
                {
                    normalPoints.Enqueue(point);
                }
                else
                {
                    newPoints.Enqueue(point);
                }
            });

            foreach (var point in newPoints)
            {
                try
                {
                    meshBuilderNew.AddSphere(point, NewPointSize);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex);
                }
            }
            foreach (var point in normalPoints)
            {
                try
                {
                    meshBuilderNormal.AddSphere(point, NormalPointSize);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex);
                }
            }

            foreach (var point3D in oldPoints)
            {
                try
                {
                    meshBuilderDeleted.AddSphere(point3D, DeletedPointSize);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex);
                }
            }

            oldPoints = currentPoints.ToArray();
            SphericalPointModel.Children.Clear();
            try
            {
                SphericalPointModel.Children.Add(new GeometryModel3D
                {
                    Geometry = meshBuilderNew.ToMesh(),
                    Material = newMaterial,
                    BackMaterial = insideMaterial
                });
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
            try
            {
                SphericalPointModel.Children.Add(new GeometryModel3D
                {
                    Geometry = meshBuilderNormal.ToMesh(),
                    Material = normalMaterial,
                    BackMaterial = insideMaterial
                });
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
            try
            {
                SphericalPointModel.Children.Add(new GeometryModel3D
                {
                    Geometry = meshBuilderDeleted.ToMesh(),
                    Material = deleteMaterial,
                    BackMaterial = insideMaterial
                });
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }

            RaisePropertyChanged("SphericalPointModel");
        }

        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        /// <value>The model.</value>
        public Model3DGroup SphericalPointModel { get; set; }

        public List<double[]> PointsSphericalCoordinates
        {
            set
            {
                pointsSphericalCoordinates = value;
                UpdatePointsData();
            }
            get { return pointsSphericalCoordinates; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string property)
        {
            var handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}