﻿using System.Windows.Controls;
using UltrasoundMonitor;

namespace RadarVisualisation.Charts3D {
    /// <summary>
    /// Interaction logic for PointsCharts.xaml
    /// </summary>
    public partial class PointsCharts : UserControl
    {
        public PointsCharts()
        {
            InitializeComponent();
        }

        public void refreshCharts(DataFlow dataFlow)
        {
            pointsDataModel.PointsSphericalCoordinates = dataFlow.PointsInSphericalCoordinates;
        }

        public void refreshSettings()
        {
            pointsDataModel.RefreshSettings();
        }
        
    }
}
