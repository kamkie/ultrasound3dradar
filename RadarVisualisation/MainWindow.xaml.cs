﻿using System;
using System.Windows;
using System.Windows.Media;
using UltrasoundMonitor;

namespace RadarVisualisation {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DataFlow dataFlow = new DataFlow(false, true);
        public static Properties.Settings settings = new Properties.Settings();

        public MainWindow()
        {
            InitializeComponent();

            CompositionTarget.Rendering += CompositionTarget_Rendering;

            settingsPanel.DataFlow = dataFlow;
            settingsPanel.MainWindow = mainWindow;
        }

        public void refreshSettings()
        {
            pointsCharts.refreshSettings();
            samplesCharts.refreshSettings();
            dftCharts.refreshSettings();
        }

        private void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            if (dataFlow.dataFresh)
            {
                dataFlow.dataFresh = false;

                pointsCharts.refreshCharts(dataFlow);
                samplesCharts.refreshCharts(dataFlow);
                dftCharts.refreshCharts(dataFlow);
            }
        }

        private void ResetCharts_Click(object sender, RoutedEventArgs e)
        {
            pointsCharts.pointsViewport.ResetCamera();
            samplesCharts.plotSamples.ResetAllAxes();
            dftCharts.plotStftCh1.ResetAllAxes();
            dftCharts.plotStftCh2.ResetAllAxes();
            dftCharts.plotStftCh3.ResetAllAxes();
            dftCharts.plotStftCh4.ResetAllAxes();
            dftCharts.plotAvgDft.ResetAllAxes();
            dftCharts.plotPis.ResetAllAxes();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            dataFlow.start();
        }
        private void Pause_Click(object sender, RoutedEventArgs e)
        {
            dataFlow.pause();
        }
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.Close();
        }

        private void Demo_Click(object sender, RoutedEventArgs e)
        {
            dataFlow.demoMode();
        }

        private void Real_Click(object sender, RoutedEventArgs e)
        {
            dataFlow.realMode();
        }
    }
}
