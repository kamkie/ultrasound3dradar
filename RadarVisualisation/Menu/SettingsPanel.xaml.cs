﻿using System;
using System.Windows;
using System.Windows.Controls;
using UltrasoundMonitor;
using static System.Globalization.CultureInfo;

namespace RadarVisualisation.Menu {
    /// <summary>
    /// Interaction logic for SettingsPanel.xaml
    /// </summary>
    public partial class SettingsPanel : UserControl
    {
        private DataFlow dataFlow;
        private MainWindow mainWindow;

        public DataFlow DataFlow
        {
            get { return dataFlow; }
            set { dataFlow = value; }
        }

        public MainWindow MainWindow
        {
            get { return mainWindow; }
            set { mainWindow = value; }
        }

        public SettingsPanel()
        {
            InitializeComponent();

            getSettings();
        }

        private void getSettings()
        {
            DataFlow.settings.Reload();

            AcceptanceThreshold.Text = Convert.ToString(DataFlow.settings.AcceptanceThreshold, InvariantCulture);
            BandStart.Text = Convert.ToString(DataFlow.settings.BandStart);
            BandStop.Text = Convert.ToString(DataFlow.settings.BandStop);
            CentralFrequency.Text = Convert.ToString(DataFlow.settings.CentralFrequency);
            DetectOnTime.Text = Convert.ToString(DataFlow.settings.DetectOnTime);
            DftLength.Text = Convert.ToString(DataFlow.settings.DftLength);
            MovingAvgLength.Text = Convert.ToString(DataFlow.settings.MovingAvgLength);
            NormalizeDataSamples.Text = Convert.ToString(DataFlow.settings.NormalizeDataSamples);
            NumberOfChanels.Text = Convert.ToString(DataFlow.settings.NumberOfChanels);
            NumberOfSamples.Text = Convert.ToString(DataFlow.settings.NumberOfSamples);
            OverlapWindow.Text = Convert.ToString(DataFlow.settings.OverlapWindow);
            PacketInvalidTreshold.Text = Convert.ToString(DataFlow.settings.PacketInvalidTreshold, InvariantCulture);
            ReciversTransmiterDistance.Text = Convert.ToString(DataFlow.settings.ReciversTransmiterDistance, InvariantCulture);
            RemoveAvgFromSamples.Text = Convert.ToString(DataFlow.settings.RemoveAvgFromSamples);
            SamplingRate.Text = Convert.ToString(DataFlow.settings.SamplingRate);
            saveFilesDirectory.Text = Convert.ToString(DataFlow.settings.saveFilesDirectory);
            SoundSpeed.Text = Convert.ToString(DataFlow.settings.SoundSpeed);
            TransmisionStartPatern.Text = Convert.ToString(DataFlow.settings.TransmisionStartPatern);
            TransmisionStopPatern.Text = Convert.ToString(DataFlow.settings.TransmisionStopPatern);
            UseHammingWindow.Text = Convert.ToString(DataFlow.settings.UseHammingWindow);
            WatchDogsTicks.Text = Convert.ToString(DataFlow.settings.WatchDogsTicks);
            DemoModeFileName.Text = Convert.ToString(DataFlow.settings.DemoModeFileName);
            DemoModeFileExtention.Text = Convert.ToString(DataFlow.settings.DemoModeFileExtention);
            DemoModeFilePath.Text = Convert.ToString(DataFlow.settings.DemoModeFilePath);
            WorkContinuous.Text = Convert.ToString(DataFlow.settings.WorkContinuous);
            IsInDemoMode.Text = Convert.ToString(DataFlow.settings.IsInDemoMode);

            NormalPointSize.Text = Convert.ToString(MainWindow.settings.NormalPointSize, InvariantCulture);
            NewPointSize.Text = Convert.ToString(MainWindow.settings.NewPointSize, InvariantCulture);
            DeletedPointSize.Text = Convert.ToString(MainWindow.settings.DeletedPointSize, InvariantCulture);
        }

        private void saveSettings()
        {
            try
            {
                DataFlow.settings.AcceptanceThreshold = Convert.ToDouble(AcceptanceThreshold.Text);
                DataFlow.settings.BandStart = Convert.ToUInt32(BandStart.Text);
                DataFlow.settings.BandStop = Convert.ToUInt32(BandStop.Text);
                DataFlow.settings.CentralFrequency = Convert.ToUInt32(CentralFrequency.Text);
                DataFlow.settings.DetectOnTime = Convert.ToBoolean(DetectOnTime.Text);
                DataFlow.settings.DftLength = Convert.ToUInt32(DftLength.Text);
                DataFlow.settings.MovingAvgLength = Convert.ToUInt32(MovingAvgLength.Text);
                DataFlow.settings.NormalizeDataSamples = Convert.ToBoolean(NormalizeDataSamples.Text);
                DataFlow.settings.NumberOfChanels = Convert.ToUInt32(NumberOfChanels.Text);
                DataFlow.settings.NumberOfSamples = Convert.ToUInt32(NumberOfSamples.Text);
                DataFlow.settings.OverlapWindow = Convert.ToUInt32(OverlapWindow.Text);
                DataFlow.settings.PacketInvalidTreshold = Convert.ToDouble(PacketInvalidTreshold.Text);
                DataFlow.settings.ReciversTransmiterDistance = Convert.ToDouble(ReciversTransmiterDistance.Text);
                DataFlow.settings.RemoveAvgFromSamples = Convert.ToBoolean(RemoveAvgFromSamples.Text);
                DataFlow.settings.SamplingRate = Convert.ToUInt32(SamplingRate.Text);
                DataFlow.settings.saveFilesDirectory = saveFilesDirectory.Text;
                DataFlow.settings.SoundSpeed = Convert.ToUInt32(SoundSpeed.Text);
                DataFlow.settings.TransmisionStartPatern = TransmisionStartPatern.Text;
                DataFlow.settings.TransmisionStopPatern = TransmisionStopPatern.Text;
                DataFlow.settings.UseHammingWindow = Convert.ToBoolean(UseHammingWindow.Text);
                DataFlow.settings.WatchDogsTicks = Convert.ToUInt32(WatchDogsTicks.Text);
                DataFlow.settings.DemoModeFileName = DemoModeFileName.Text;
                DataFlow.settings.DemoModeFileExtention = DemoModeFileExtention.Text;
                DataFlow.settings.DemoModeFilePath = DemoModeFilePath.Text;
                DataFlow.settings.WorkContinuous = Convert.ToBoolean(WorkContinuous.Text);
                DataFlow.settings.IsInDemoMode = Convert.ToBoolean(IsInDemoMode.Text);

                MainWindow.settings.DeletedPointSize = Convert.ToDouble(DeletedPointSize.Text);
                MainWindow.settings.NewPointSize = Convert.ToDouble(NewPointSize.Text);
                MainWindow.settings.NormalPointSize = Convert.ToDouble(NormalPointSize.Text);

                DataFlow.settings.Save();
                dataFlow.refreshSettings();
                mainWindow.refreshSettings();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            saveSettings();
        }

        private void Cancel_Button_Click_1(object sender, RoutedEventArgs e)
        {
            getSettings();
        }

        private void Reset_Button_Click_1(object sender, RoutedEventArgs e)
        {
            DataFlow.settings.Reset();
            saveSettings();
            getSettings();
        }
    }
}
