clear all;
% clc;
close all
[FileName,PathName,FilterIndex] = uigetfile('*.csv','Open files','','MultiSelect','on');

if  iscellstr(FileName)
    fileCount=size(FileName,2);
else
	fileCount=1;
end;
for i=1:fileCount
if iscellstr(FileName)
    fileName = FileName{i};
else    
    fileName = FileName;
end;
[num, txt, all] = xlsread(fileName);
figure(i);

set(gcf,'name',fileName,'numbertitle','off')
subplot(1,2,1);
imagesc(10*log10(num'));
subplot(1,2,2);
imagesc(num');
end;

