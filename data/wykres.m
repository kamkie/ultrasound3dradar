clear all;
% clc;
close all
[FileName,PathName,FilterIndex] = uigetfile('*.csv','Open files','','MultiSelect','on');
% filterPG1=filterPG();
FileName
if  iscellstr(FileName)
    fileCount=size(FileName,2);
else
	fileCount=1;
end;
for i=1:fileCount
if iscellstr(FileName)
    %num = csvread(FileName{i});
    [num, txt, all] = xlsread(FileName{i});
else    
    %num = csvread(FileName);
    [num, txt, all] = xlsread(FileName);
end;

% moving average filter
a = 1;
b = [1/4 1/4 1/4 1/4]; 
% gauss filter
% sigma = 5;
% sizeFilter = 30;
% x = linspace(-sizeFilter / 2, sizeFilter / 2, sizeFilter);
% gaussFilter = exp(-x .^ 2 / (2 * sigma ^ 2));
% gaussFilter = gaussFilter / sum (gaussFilter); % normalize
% b=gaussFilter;
% a=1;


% no filter
y1 = num(:,1);
y2 = num(:,2);
y3 = num(:,3);
y4 = num(:,4);

figure(i);
%samples
subplot(2,3,1);
hold on;
plot(y1,'r');
plot(y2,'g');
plot(y3,'b');
plot(y4,'y');
hold off;



subplot(2,3,2);
fs=550e3;
[S1,F1,T1,P1] = spectrogram(y1,256,250,256,fs);
surf(T1,F1,10*log10(P1),'edgecolor','none'); axis tight; 
view(0,90);
xlabel('Time (Seconds)'); ylabel('Hz');
subplot(2,3,3);
[S2,F2,T2,P2] = spectrogram(y2,256,250,256,fs);
surf(T2,F2,10*log10(P2),'edgecolor','none'); axis tight; 
view(0,90);
xlabel('Time (Seconds)'); ylabel('Hz');
subplot(2,3,5);
[S3,F3,T3,P3] = spectrogram(y3,256,250,256,fs);
surf(T3,F3,10*log10(P3),'edgecolor','none'); axis tight; 
view(0,90);
xlabel('Time (Seconds)'); ylabel('Hz');
subplot(2,3,6);
[S4,F4,T4,P4] = spectrogram(y4,256,250,256,fs);
surf(T4,F4,10*log10(P4),'edgecolor','none'); axis tight; 
view(0,90);
xlabel('Time (Seconds)'); ylabel('Hz');

% %with filter
y1 = filter(b,a,y1);
y2 = filter(b,a,y2);
y3 = filter(b,a,y3);
y4 = filter(b,a,y4);

% filtered
subplot(2,3,4);
hold on;
plot(y1,'r');
plot(y2,'g');
plot(y3,'b');
plot(y4,'y');
hold off;
end;

