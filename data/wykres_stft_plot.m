clear all;
% clc;
close all
[FileName,PathName,FilterIndex] = uigetfile('*.csv','Open files','','MultiSelect','on');

if  iscellstr(FileName)
    fileCount=size(FileName,2);
else
	fileCount=1;
end;
for i=1:fileCount
if iscellstr(FileName)
    fileName = FileName{i};
else    
    fileName = FileName;
end;
[num, txt, all] = xlsread(fileName);

y1 = num(:,1);
y2 = num(:,2);
y3 = num(:,3);
y4 = num(:,4);

figure(i);
set(gcf,'name',fileName,'numbertitle','off')
hold on;
plot(y1,'r');
plot(y2,'g');
plot(y3,'b');
plot(y4,'y')
hold off;
end;

